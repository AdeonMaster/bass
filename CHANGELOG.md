### Changelog

- Added missing BASS C API parts into squirrel
- Added ability to read sound files from VDF archives placed in `SOUND_DIR`
- Documented unexposed API parts in `UNEXPOSED_API.md`