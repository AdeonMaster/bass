cmake_minimum_required(VERSION 3.17)

project(Bass)

file(GLOB SRC
	"include/*.h"
)

add_library(Bass INTERFACE)
target_sources(Bass INTERFACE ${SRC})

target_include_directories(Bass
	INTERFACE
		"include/"
)

target_link_libraries(Bass INTERFACE "${CMAKE_CURRENT_SOURCE_DIR}/lib/bass.lib")