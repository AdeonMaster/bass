﻿#include "sqratGlobalFuncStdcall.h"
#include "sqFunction.h"

#include <bass.h>
#include "bass_error_message.h"

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	SqModule::Initialize(vm, api);
	Sqrat::DefaultVM::Set(vm);

	Sqrat::Class<BASS_SAMPLE> bass_sample(vm, "BassSample");

	Sqrat::RootTable roottable(vm);
	roottable.Bind("BassSample", bass_sample);

	roottable
		/* squirreldoc (func)
		*
		* This function sets the value of a config option.
		*
		* @version	0.2
		* @category Config
		* @side		client
		* @name		BASS_SetConfig
		* @param	(int) option the option to set the value. For more information see [Config constants](../../../client-constants/config/)
		* @param	(int) value the new option value.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_SetConfig", BASS_SetConfig)
		/* squirreldoc (func)
		*
		* This function gets the value of a config option.
		*
		* @version	0.2
		* @category Config
		* @side		client
		* @name		BASS_GetConfig
		* @param	(int) option the option to set the value. For more information see [Config constants](../../../client-constants/config/)
		* @return	(int) If successful, the value of the requested config option is returned, else `-1` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_GetConfig", BASS_GetConfig)
		/* squirreldoc (func)
		*
		* This function sets the value of a pointer config option.
		*
		* @version	0.2
		* @category Config
		* @side		client
		* @name		BASS_SetConfigPtr
		* @param	(int) option the option to set the value, can be `BASS_CONFIG_NET_AGENT` or `BASS_CONFIG_NET_PROXY`. For more information see [Config constants](../../../client-constants/config/).
		* @param	(string) value the new option value.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_SetConfigPtr", sq_bass_setconfigptr, 3, ".is")
		/* squirreldoc (func)
		*
		* This function gets the value of a pointer config option.
		*
		* @version	0.2
		* @category Config
		* @side		client
		* @name		BASS_GetConfigPtr
		* @param	(int) option the option to set the value, can be `BASS_CONFIG_NET_AGENT` or `BASS_CONFIG_NET_PROXY`. For more information see [Config constants](../../../client-constants/config/).
		* @return	(bool) If successful, the value of the requested config option is returned, else `null` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_GetConfigPtr", sq_bass_getconfigptr, 2, ".i")
		/* squirreldoc (func)
		*
		* This function gets the version of BASS that is loaded.
		*
		* @version	0.2
		* @category Info
		* @side		client
		* @name		BASS_GetVersion
		* @return	(int) The BASS version. For example, `0x02040103` (hex), would be version 2.4.1.3 
		*
		*/
		.Func("BASS_GetVersion", BASS_GetVersion)
		/* squirreldoc (func)
		*
		* This function gets the error code for most recent BASS function call in the current thread.
		*
		* @version	0.2
		* @category Error
		* @side		client
		* @name		BASS_ErrorGetCode
		* @return	(int) The error code. For more information see [Error constants](../../../client-constants/error/).
		*
		*/
		.Func("BASS_ErrorGetCode", BASS_ErrorGetCode)
		/* squirreldoc (func)
		*
		* This function gets the error message for most recent BASS function call in the current thread.
		*
		* @version	0.2
		* @category Error
		* @side		client
		* @name		BASS_ErrorGetMessage
		* @return	(string) The error message.
		*
		*/
		.Func("BASS_ErrorGetMessage", BASS_ErrorGetMessage)
		/* squirreldoc (func)
		*
		* This function gets the information about the output device.
		*
		* @version	0.2
		* @category Info
		* @side		client
		* @name		BASS_GetDeviceInfo
		* @param	(int) device the device to get the information of... `0` = first.
		* @return	({name, driver, flags}) The information about certain device or `null` if device doesn't exists.
		*
		*/
		.SquirrelFunc("BASS_GetDeviceInfo", sq_bass_getdeviceinfo, 2, ".i")
		/* squirreldoc (func)
		*
		* This function initializes an output device.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_Init
		* @param	(int) device the device to use... `-1` = default device, `0` = no sound, `1` = first real.
		* @param	(int) freq ouput sample rate.
		* @param	(int) flags a combination of device flags. For more information see [Device constants](../../../client-constants/device/).
		* @return	(bool) If the device was successfully initialized, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_Init", sq_bass_init, 4, ".iii")
		/* squirreldoc (func)
		*
		* This function sets the device to use for subsequent calls in the current thread.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_SetDevice
		* @param	(int) device the device to use... `0` = no sound, `1` = first real output device.
		* @return	(bool) If successful, then `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_SetDevice", BASS_SetDevice)
		/* squirreldoc (func)
		*
		* This function gets the device setting of the current thread.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_GetDevice
		* @return	(bool) If successful, the device number is returned, else `-1` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_GetDevice", BASS_GetDevice)
		/* squirreldoc (func)
		*
		* This function frees all resources used by the output device, including all its samples, streams and MOD musics. Call it only when closing the game.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_Free
		* @return	(bool) If successful, then `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_Free", BASS_Free)
		/* squirreldoc (func)
		*
		* This function gets a pointer to a DirectSound object interface.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_GetDSoundObject
		* @param	(int) object the interface to retrieve channel/music/stream handle or [BASS_OBJECT_DS](../../../client-constants/object/) or [BASS_OBJECT_DS3DL](../../../client-constants/object/).
		* @return	(userpointer) If successful, then a pointer to the requested object is returned, else `nullptr` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_GetDSoundObject", BASS_GetDSoundObject)
		/* squirreldoc (func)
		*
		* This function gets the information on the device being used.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_GetInfo
		* @return	({flags, hwsize, hwfree, freesam, free3d, minrate, maxrate, eax, minbuf, dsver, latency, initflags, speakers, freq}) If successful, returns a structure containing information about used device, else `null` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_GetInfo", sq_bass_getinfo)
		/* squirreldoc (func)
		*
		* This function updates the stream and music channel playback buffers.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_Update
		* @param	(int) length the amount of data to render, in miliseconds.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_Update", BASS_Update)
		/* squirreldoc (func)
		*
		* This function gets the current CPU usage of BASS.
		*
		* @version	0.2
		* @category Info
		* @side		client
		* @name		BASS_GetCPU
		* @return	(float) the BASS CPU usage as percentage.
		*
		*/
		.Func("BASS_GetCPU", BASS_GetCPU)
		/* squirreldoc (func)
		*
		* This function starts (or resumes) the output.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_Start
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_Start", BASS_Start)
		/* squirreldoc (func)
		*
		* This function stops the output, stopping all musics/samples/streams on it.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_Stop
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_Stop", BASS_Stop)
		/* squirreldoc (func)
		*
		* This function stops the output, pausing all musics/samples/streams on it.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_Pause
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_Pause", BASS_Pause)
		/* squirreldoc (func)
		*
		* This function sets the output master volume.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_SetVolume
		* @note		This function affects the volume level of all applications using the same output device. If you wish to only affect the level of your application's sounds, the `BASS_ATTRIB_VOL` attribute and/or the `BASS_CONFIG_GVOL_MUSIC` / `BASS_CONFIG_GVOL_SAMPLE` / `BASS_CONFIG_GVOL_STREAM` config options should be used instead. 
		* @param	(int) volume the volume level... `0.0` (silent) to `1.0` (max). 
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_SetVolume", BASS_SetVolume)
		/* squirreldoc (func)
		*
		* This function gets the output master volume.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_GetVolume
		* @return	(float) If successful, the volume level is returned, else `-1` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_GetVolume", BASS_GetVolume)

		/* squirreldoc (func)
		*
		* This function sets the factors that affect the calculations of 3D sound.
		*
		* @version	0.2
		* @category Surrounding
		* @side		client
		* @name		BASS_Set3DFactors
		* @param	(float) distf the distance factor... `0.0` or less = leave current... examples: `1.0` = use meters, `0.9144` = use yards, `0.3048` = use feet. By default BASS measures distances in meters, you can change this setting if you are using a different unit of measurement.
		* @param	(float) rollf the rolloff factor, how fast the sound quietens with distance... `0.0` (min) - `10.0` (max), less than `0.0` = leave current... examples: `0.0` = no rolloff, `1.0` = real world, `2.0` = 2x real.
		* @param	(float) doppf the doppler factor... `0.0` (min) - `10.0` (max), less than `0.0` = leave current... examples: `0.0` = no doppler, `1.0` = real world, `2.0` = 2x real.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_Set3DFactors", BASS_Set3DFactors)
		/* squirreldoc (func)
		*
		* This function gets the factors that affect the calculations of 3D sound.
		*
		* @version	0.2
		* @category Surrounding
		* @side		client
		* @name		BASS_Get3DFactors
		* @return	({distf, rollf, doppf}) If successful, the factors information is returned, else `null` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_Get3DFactors", sq_bass_get3dfactors, 1, ".")
		/* squirreldoc (func)
		*
		* This fucntion sets the position, velocity, and orientation of the listener (ie. the player).
		*
		* @version	0.2
		* @category Surrounding
		* @side		client
		* @name		BASS_Set3DPosition
		* @param	(Vec3) pos the position of the listener.
		* @param	(Vec3) vel the listener's velocity.
		* @param	(Vec3) front the listener's poinitng direction.
		* @param	(Vec3) top the listener's up direction.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_Set3DPosition", sq_bass_set3dposition, 5, ".")
		/* squirreldoc (func)
		*
		* This function gets the position, velocity, and orientation of the listener (ie. the player).
		*
		* @version	0.2
		* @category Surrounding
		* @side		client
		* @name		BASS_Get3DPosition
		* @return	({pos, vel, front, top}) If successful, the 3d position vectors are returned, else `null` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_Get3DPosition", sq_bass_get3dposition, 1, ".")
		/* squirreldoc (func)
		*
		* This function applies changes made to the 3D system.
		*
		* @version	0.2
		* @category Surrounding
		* @side		client
		* @name		BASS_Apply3D
		*
		*/
		.Func("BASS_Apply3D", BASS_Apply3D)
		/* squirreldoc (func)
		*
		* This function sets the type of EAX environment and its parameters. 
		*
		* @version	0.2
		* @category Surrounding
		* @side		client
		* @name		BASS_SetEAXParameters
		* @param	(int) env the eax enviorment... `-1` = leave current. For more information see [EAX constants](../../../client-constants/eax/)
		* @param	(float) vol the volume of the reverb... `0.0` (off) to `1.0` (max), less than `0.0` = leave current. .
		* @param	(float) decay the time in seconds it takes the reverb to diminish by 60 dB... `0.1` (min) to `20.0` (max), less than `0.0` = leave current.
		* @param	(float) damp the time in seconds it takes the reverb to diminish by 60 dB... `0.1` (min) to 20 (max), less than `0.0` = leave current. 
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_SetEAXParameters", BASS_SetEAXParameters)
		/* squirreldoc (func)
		*
		* This function gets the current type of EAX environment and its parameters.
		*
		* @version	0.2
		* @category Surrounding
		* @side		client
		* @name		BASS_GetEAXParameters
		* @return	({env, vol, decay, damp}) If successful, the eax informations are returned, else `null` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_GetEAXParameters", sq_bass_geteaxparameters, 1, ".")

		.SquirrelFunc("BASS_MusicLoad", sq_bass_musicload, 6, ".siiii")
		.Func("BASS_MusicFree", BASS_MusicFree)

		.SquirrelFunc("BASS_SampleLoad", sq_bass_sampleload, 6, ".siiii")
		.Func("BASS_SampleCreate", BASS_SampleCreate)
		.Func("BASS_SampleFree", BASS_SampleFree)
		.SquirrelFunc("BASS_SampleGetInfo", sq_bass_samplegetinfo, 2, ".i")
		.SquirrelFunc("BASS_SampleSetInfo", sq_bass_samplesetinfo, 3, ".ix")
		.Func("BASS_SampleGetChannel", BASS_SampleGetChannel)
		.SquirrelFunc("BASS_SampleGetChannels", sq_bass_samplegetchannels, 2, ".i")
		.Func("BASS_SampleStop", BASS_SampleStop)

		.SquirrelFunc("BASS_StreamCreateFile", sq_bass_streamcreatefile, 3, ".si")
		.SquirrelFunc("BASS_StreamCreateURL", sq_bass_streamcreateurl, 4, ".sii")
		.Func("BASS_StreamFree", BASS_StreamFree)
		.Func("BASS_StreamGetFilePosition", BASS_StreamGetFilePosition)

		.Func("BASS_ChannelBytes2Seconds", BASS_ChannelBytes2Seconds)
		.Func("BASS_ChannelSeconds2Bytes", BASS_ChannelSeconds2Bytes)
		.Func("BASS_ChannelGetDevice", BASS_ChannelGetDevice)
		.Func("BASS_ChannelSetDevice", BASS_ChannelSetDevice)
		.Func("BASS_ChannelIsActive", BASS_ChannelIsActive)
		.SquirrelFunc("BASS_ChannelGetInfo", sq_bass_channelgetinfo, 2, ".i")
		.Func("BASS_ChannelGetTags", BASS_ChannelGetTags)
		.Func("BASS_ChannelFlags", BASS_ChannelFlags)
		.Func("BASS_ChannelUpdate", BASS_ChannelUpdate)
		.Func("BASS_ChannelLock", BASS_ChannelLock)
		.Func("BASS_ChannelPlay", BASS_ChannelPlay)
		.Func("BASS_ChannelStop", BASS_ChannelStop)
		.Func("BASS_ChannelPause", BASS_ChannelPause)
		.Func("BASS_ChannelSetAttribute", BASS_ChannelSetAttribute)
		.SquirrelFunc("BASS_ChannelGetAttribute", sq_bass_channelgetattribute, 3, ".ii")
		.Func("BASS_ChannelSlideAttribute", BASS_ChannelSlideAttribute)
		.Func("BASS_ChannelIsSliding", BASS_ChannelIsSliding)
		.Func("BASS_ChannelSetAttributeEx", BASS_ChannelSetAttributeEx)
		.Func("BASS_ChannelGetAttributeEx", BASS_ChannelGetAttributeEx)
		.Func("BASS_ChannelSet3DAttributes", BASS_ChannelSet3DAttributes)
		.SquirrelFunc("BASS_ChannelGet3DAttributes", sq_bass_channelget3dattributes, 2, ".i")
		.SquirrelFunc("BASS_ChannelSet3DPosition", sq_bass_channelset3dposition, 5, ".i")
		.SquirrelFunc("BASS_ChannelGet3DPosition", sq_bass_channelget3dposition, 2, ".i")
		.Func("BASS_ChannelGetLength", BASS_ChannelGetLength)
		.Func("BASS_ChannelSetPosition", BASS_ChannelSetPosition)
		.Func("BASS_ChannelGetPosition", BASS_ChannelGetPosition)
		.Func("BASS_ChannelGetLevel", BASS_ChannelGetLevel)
		.SquirrelFunc("BASS_ChannelGetLevelEx", sq_bass_channelgetlevelex, 4, ".ifi")
		.Func("BASS_ChannelSetLink", BASS_ChannelSetLink)
		.Func("BASS_ChannelRemoveLink", BASS_ChannelRemoveLink)
		.Func("BASS_ChannelSetFX", BASS_ChannelSetFX)
		.Func("BASS_ChannelRemoveFX", BASS_ChannelRemoveFX)
	;

	Sqrat::ConstTable(vm)
		/* squirreldoc (const)
		*
		* Represents current BASS Version as integer.
		*
		* @category Info
		* @side		client
		* @name		BASSVERSION
		*
		*/
		.Const("BASSVERSION", BASSVERSION)

		/* squirreldoc (const)
		*
		* Represents current BASS Version as text.
		*
		* @category Info
		* @side		client
		* @name		BASSVERSIONTEXT
		*
		*/
		.Const("BASSVERSIONTEXT", BASSVERSIONTEXT)

		/* squirreldoc (const)
		*
		* Represents ok status (no error).
		*
		* @category Error
		* @side		client
		* @name		BASS_OK
		*
		*/
		.Const("BASS_OK", BASS_OK)
		/* squirreldoc (const)
		*
		* Represents insufficient memory error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_MEM
		*
		*/
		.Const("BASS_ERROR_MEM", BASS_ERROR_MEM)
		/* squirreldoc (const)
		*
		* Represents unable to open file error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_FILEOPEN
		*
		*/
		.Const("BASS_ERROR_FILEOPEN", BASS_ERROR_FILEOPEN)
		/* squirreldoc (const)
		*
		* Represents no device driver or device already used error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_DRIVER
		*
		*/
		.Const("BASS_ERROR_DRIVER", BASS_ERROR_DRIVER)
		/* squirreldoc (const)
		*
		* Represents sample buffer lost error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_BUFLOST
		*
		*/
		.Const("BASS_ERROR_BUFLOST", BASS_ERROR_BUFLOST)
		/* squirreldoc (const)
		*
		* Represents invalid handle passed error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_HANDLE
		*
		*/
		.Const("BASS_ERROR_HANDLE", BASS_ERROR_HANDLE)
		/* squirreldoc (const)
		*
		* Represents sample format not being supported by the device/drivers error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_FORMAT
		*
		*/
		.Const("BASS_ERROR_FORMAT", BASS_ERROR_FORMAT)
		/* squirreldoc (const)
		*
		* Represents invalid position passed error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_POSITION
		*
		*/
		.Const("BASS_ERROR_POSITION", BASS_ERROR_POSITION)
		/* squirreldoc (const)
		*
		* Represents device not initialized error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_INIT
		*
		*/
		.Const("BASS_ERROR_INIT", BASS_ERROR_INIT)
		/* squirreldoc (const)
		*
		* Represents output is paused/stopped error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_START
		*
		*/
		.Const("BASS_ERROR_START", BASS_ERROR_START)
		/* squirreldoc (const)
		*
		* Represents SSL/HTTPS support is not available error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_SSL
		*
		*/
		.Const("BASS_ERROR_SSL", BASS_ERROR_SSL)
		/* squirreldoc (const)
		*
		* Represents certain action was done already error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_ALREADY
		*
		*/
		.Const("BASS_ERROR_ALREADY", BASS_ERROR_ALREADY)
		/* squirreldoc (const)
		*
		* Represents sample has no free channels error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_NOCHAN
		*
		*/
		.Const("BASS_ERROR_NOCHAN", BASS_ERROR_NOCHAN)
		/* squirreldoc (const)
		*
		* Represents illegal type being passed error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_ILLTYPE
		*
		*/
		.Const("BASS_ERROR_ILLTYPE", BASS_ERROR_ILLTYPE)
		/* squirreldoc (const)
		*
		* Represents illegal parameter being passed error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_ILLPARAM
		*
		*/
		.Const("BASS_ERROR_ILLPARAM", BASS_ERROR_ILLPARAM)
		/* squirreldoc (const)
		*
		* Represents 3d functionallity not initialized/supported error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_NO3D
		*
		*/
		.Const("BASS_ERROR_NO3D", BASS_ERROR_NO3D)
		/* squirreldoc (const)
		*
		* Represents EAX functionallity not supported error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_NOEAX
		*
		*/
		.Const("BASS_ERROR_NOEAX", BASS_ERROR_NOEAX)
		/* squirreldoc (const)
		*
		* Represents invalid device error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_DEVICE
		*
		*/
		.Const("BASS_ERROR_DEVICE", BASS_ERROR_DEVICE)
		/* squirreldoc (const)
		*
		* Represents channel not playing error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_NOPLAY
		*
		*/
		.Const("BASS_ERROR_NOPLAY", BASS_ERROR_NOPLAY)
		/* squirreldoc (const)
		*
		* Represents illegal sample rate error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_FREQ
		*
		*/
		.Const("BASS_ERROR_FREQ", BASS_ERROR_FREQ)
		/* squirreldoc (const)
		*
		* Represents stream is not a file stream error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_NOTFILE
		*
		*/
		.Const("BASS_ERROR_NOTFILE", BASS_ERROR_NOTFILE)
		/* squirreldoc (const)
		*
		* Represents no hardware voices available error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_NOHW
		*
		*/
		.Const("BASS_ERROR_NOHW", BASS_ERROR_NOHW)
		/* squirreldoc (const)
		*
		* Represents MOD music has no sequence data error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_EMPTY
		*
		*/
		.Const("BASS_ERROR_EMPTY", BASS_ERROR_EMPTY)
		/* squirreldoc (const)
		*
		* Represents no internet connection could be opened error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_NONET
		*
		*/
		.Const("BASS_ERROR_NONET", BASS_ERROR_NONET)
		/* squirreldoc (const)
		*
		* Represents couldn't create file error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_CREATE
		*
		*/
		.Const("BASS_ERROR_CREATE", BASS_ERROR_CREATE)
		/* squirreldoc (const)
		*
		* Represents effects are not available error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_NOFX
		*
		*/
		.Const("BASS_ERROR_NOFX", BASS_ERROR_NOFX)
		/* squirreldoc (const)
		*
		* Represents requested data is not available error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_NOTAVAIL
		*
		*/
		.Const("BASS_ERROR_NOTAVAIL", BASS_ERROR_NOTAVAIL)
		/* squirreldoc (const)
		*
		* Represents channel is/isn't a "decoding channel" error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_DECODE
		*
		*/
		.Const("BASS_ERROR_DECODE", BASS_ERROR_DECODE)
		/* squirreldoc (const)
		*
		* Represents sufficient version of DirectX is not installed error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_DX
		*
		*/
		.Const("BASS_ERROR_DX", BASS_ERROR_DX)
		/* squirreldoc (const)
		*
		* Represents connection/sample timedout error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_TIMEOUT
		*
		*/
		.Const("BASS_ERROR_TIMEOUT", BASS_ERROR_TIMEOUT)
		/* squirreldoc (const)
		*
		* Represents file format not recognised/supported error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_FILEFORM
		*
		*/
		.Const("BASS_ERROR_FILEFORM", BASS_ERROR_FILEFORM)
		/* squirreldoc (const)
		*
		* Represents speaker unavailable error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_SPEAKER
		*
		*/
		.Const("BASS_ERROR_SPEAKER", BASS_ERROR_SPEAKER)
		/* squirreldoc (const)
		*
		* Represents invalid BASS version (used by add-ons) error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_VERSION
		*
		*/
		.Const("BASS_ERROR_VERSION", BASS_ERROR_VERSION)
		/* squirreldoc (const)
		*
		* Represents codec is not available/supported error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_CODEC
		*
		*/
		.Const("BASS_ERROR_CODEC", BASS_ERROR_CODEC)
		/* squirreldoc (const)
		*
		* Represents channel/file has ended error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_ENDED
		*
		*/
		.Const("BASS_ERROR_ENDED", BASS_ERROR_ENDED)
		/* squirreldoc (const)
		*
		* Represents device is busy error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_BUSY
		*
		*/
		.Const("BASS_ERROR_BUSY", BASS_ERROR_BUSY)
		/* squirreldoc (const)
		*
		* Represents unknown error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_BUSY
		*
		*/
		.Const("BASS_ERROR_UNKNOWN", BASS_ERROR_UNKNOWN)

		/* squirreldoc (const)
		*
		* Represents playback buffer length.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_BUFFER
		*
		*/
		.Const("BASS_CONFIG_BUFFER", BASS_CONFIG_BUFFER)
		/* squirreldoc (const)
		*
		* Represents update period of playback buffers.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_UPDATEPERIOD
		*
		*/
		.Const("BASS_CONFIG_UPDATEPERIOD", BASS_CONFIG_UPDATEPERIOD)
		/* squirreldoc (const)
		*
		* Represents global sample volume.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_GVOL_SAMPLE
		*
		*/
		.Const("BASS_CONFIG_GVOL_SAMPLE", BASS_CONFIG_GVOL_SAMPLE)
		/* squirreldoc (const)
		*
		* Represents global stream volume.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_GVOL_STREAM
		*
		*/
		.Const("BASS_CONFIG_GVOL_STREAM", BASS_CONFIG_GVOL_STREAM)
		/* squirreldoc (const)
		*
		* Represents global MOD music volume.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_GVOL_MUSIC
		*
		*/
		.Const("BASS_CONFIG_GVOL_MUSIC", BASS_CONFIG_GVOL_MUSIC)
		/* squirreldoc (const)
		*
		* Represents volume translation curve.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_CURVE_VOL
		*
		*/
		.Const("BASS_CONFIG_CURVE_VOL", BASS_CONFIG_CURVE_VOL)
		/* squirreldoc (const)
		*
		* Represents panning translation curve.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_CURVE_PAN
		*
		*/
		.Const("BASS_CONFIG_CURVE_PAN", BASS_CONFIG_CURVE_PAN)
		/* squirreldoc (const)
		*
		* Represents passing 32-bit floating-point sample data to all DSP functions.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_FLOATDSP
		*
		*/
		.Const("BASS_CONFIG_FLOATDSP", BASS_CONFIG_FLOATDSP)
		/* squirreldoc (const)
		*
		* Represents the 3D algorithm for software mixed 3D channels.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_3DALGORITHM
		*
		*/
		.Const("BASS_CONFIG_3DALGORITHM", BASS_CONFIG_3DALGORITHM)
		/* squirreldoc (const)
		*
		* Represents time to wait for a server to respond to a connection request.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_NET_TIMEOUT
		*
		*/
		.Const("BASS_CONFIG_NET_TIMEOUT", BASS_CONFIG_NET_TIMEOUT)
		/* squirreldoc (const)
		*
		* Represents internet download buffer length.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_NET_BUFFER
		*
		*/
		.Const("BASS_CONFIG_NET_BUFFER", BASS_CONFIG_NET_BUFFER)
		/* squirreldoc (const)
		*
		* Represents preventing channels being played when the output is paused.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_PAUSE_NOPLAY
		*
		*/
		.Const("BASS_CONFIG_PAUSE_NOPLAY", BASS_CONFIG_PAUSE_NOPLAY)
		/* squirreldoc (const)
		*
		* Represents amount to pre-buffer when opening internet streams.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_NET_PREBUF
		*
		*/
		.Const("BASS_CONFIG_NET_PREBUF", BASS_CONFIG_NET_PREBUF)
		/* squirreldoc (const)
		*
		* Represents using passive mode in FTP connections.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_NET_PASSIVE
		*
		*/
		.Const("BASS_CONFIG_NET_PASSIVE", BASS_CONFIG_NET_PASSIVE)
		/* squirreldoc (const)
		*
		* Represents processing URLs in playlists.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_NET_PLAYLIST
		*
		*/
		.Const("BASS_CONFIG_NET_PLAYLIST", BASS_CONFIG_NET_PLAYLIST)
		/* squirreldoc (const)
		*
		* Represents maximum number of virtual channels to use in the rendering of IT files.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_MUSIC_VIRTUAL
		*
		*/
		.Const("BASS_CONFIG_MUSIC_VIRTUAL", BASS_CONFIG_MUSIC_VIRTUAL)
		/* squirreldoc (const)
		*
		* Represents the amount of data to check in order to verify/detect the file format.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_VERIFY
		*
		*/
		.Const("BASS_CONFIG_VERIFY", BASS_CONFIG_VERIFY)
		/* squirreldoc (const)
		*
		* Represents the number of threads to use for updating playback buffers.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_UPDATETHREADS
		*
		*/
		.Const("BASS_CONFIG_UPDATETHREADS", BASS_CONFIG_UPDATETHREADS)
		/* squirreldoc (const)
		*
		* Represents the output device buffer length.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_DEV_BUFFER
		*
		*/
		.Const("BASS_CONFIG_DEV_BUFFER", BASS_CONFIG_DEV_BUFFER)
		/* squirreldoc (const)
		*
		* Represents enabling true play position mode on Windows Vista and newer.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_VISTA_TRUEPOS
		*
		*/
		.Const("BASS_CONFIG_VISTA_TRUEPOS", BASS_CONFIG_VISTA_TRUEPOS)
		/* squirreldoc (const)
		*
		* Represents inclusion of a `Default` entry in the output device list.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_DEV_DEFAULT
		*
		*/
		.Const("BASS_CONFIG_DEV_DEFAULT", BASS_CONFIG_DEV_DEFAULT)
		/* squirreldoc (const)
		*
		* Represents the time to wait for a server to deliver more data for an internet stream.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_NET_READTIMEOUT
		*
		*/
		.Const("BASS_CONFIG_NET_READTIMEOUT", BASS_CONFIG_NET_READTIMEOUT)
		/* squirreldoc (const)
		*
		* Represents enabling speaker assignment with panning/balance control on Windows Vista and newer.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_VISTA_SPEAKERS
		*
		*/
		.Const("BASS_CONFIG_VISTA_SPEAKERS", BASS_CONFIG_VISTA_SPEAKERS)
		/* squirreldoc (const)
		*
		* Represents disabling the use of Media Foundation.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_MF_DISABLE
		*
		*/
		.Const("BASS_CONFIG_MF_DISABLE", BASS_CONFIG_MF_DISABLE)
		/* squirreldoc (const)
		*
		* Represents the number of existing HMUSIC / HRECORD / HSAMPLE / HSTREAM handle.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_HANDLES
		*
		*/
		.Const("BASS_CONFIG_HANDLES", BASS_CONFIG_HANDLES)
		/* squirreldoc (const)
		*
		* Represents the usage of Unicode character set in device information.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_UNICODE
		*
		*/
		.Const("BASS_CONFIG_UNICODE", BASS_CONFIG_UNICODE)
		/* squirreldoc (const)
		*
		* Represents the default sample rate conversion quality.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_SRC
		*
		*/
		.Const("BASS_CONFIG_SRC", BASS_CONFIG_SRC)
		/* squirreldoc (const)
		*
		* Represents the default sample rate conversion quality for samples.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_SRC_SAMPLE
		*
		*/
		.Const("BASS_CONFIG_SRC_SAMPLE", BASS_CONFIG_SRC_SAMPLE)
		/* squirreldoc (const)
		*
		* Represents the buffer length for asynchronous file reading.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_ASYNCFILE_BUFFER
		*
		*/
		.Const("BASS_CONFIG_ASYNCFILE_BUFFER", BASS_CONFIG_ASYNCFILE_BUFFER)
		/* squirreldoc (const)
		*
		* Represents Pre-scanning chained OGG files.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_OGG_PRESCAN
		*
		*/
		.Const("BASS_CONFIG_OGG_PRESCAN", BASS_CONFIG_OGG_PRESCAN)
		/* squirreldoc (const)
		*
		* Represents playing the audio from video files using Media Foundation.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_MF_VIDEO
		*
		*/
		.Const("BASS_CONFIG_MF_VIDEO", BASS_CONFIG_MF_VIDEO)
		/* squirreldoc (const)
		*
		* Represents not stopping the output device when nothing is playing on it.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_DEV_NONSTOP
		*
		*/
		.Const("BASS_CONFIG_DEV_NONSTOP", BASS_CONFIG_DEV_NONSTOP)
		/* squirreldoc (const)
		*
		* Represents the amount of data to check in order to verify/detect the file format of internet streams.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_VERIFY_NET
		*
		*/
		.Const("BASS_CONFIG_VERIFY_NET", BASS_CONFIG_VERIFY_NET)

		/* squirreldoc (const)
		*
		* Represents the "User-Agent" request header sent to servers.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_NET_AGENT
		*
		*/
		.Const("BASS_CONFIG_NET_AGENT", BASS_CONFIG_NET_AGENT)
		/* squirreldoc (const)
		*
		* Represents the proxy server settings.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_NET_PROXY
		*
		*/
		.Const("BASS_CONFIG_NET_PROXY", BASS_CONFIG_NET_PROXY)

		/* squirreldoc (const)
		*
		* Represents using 8-bit resolution.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_8BITS
		*
		*/
		.Const("BASS_DEVICE_8BITS", BASS_DEVICE_8BITS)
		/* squirreldoc (const)
		*
		* Represents using mono.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_MONO
		*
		*/
		.Const("BASS_DEVICE_MONO", BASS_DEVICE_MONO)
		/* squirreldoc (const)
		*
		* Represents enabling 3d functionallity.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_3D
		*
		*/
		.Const("BASS_DEVICE_3D", BASS_DEVICE_3D)
		/* squirreldoc (const)
		*
		* Represents calculating device latency (BASS_GetInfo).
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_LATENCY
		*
		*/
		.Const("BASS_DEVICE_LATENCY", BASS_DEVICE_LATENCY)
		/* squirreldoc (const)
		*
		* Represents detecting speakers via Windows control panel.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_CPSPEAKERS
		*
		*/
		.Const("BASS_DEVICE_CPSPEAKERS", BASS_DEVICE_CPSPEAKERS)
		/* squirreldoc (const)
		*
		* Represents force enabling of speaker assignment.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_SPEAKERS
		*
		*/
		.Const("BASS_DEVICE_SPEAKERS", BASS_DEVICE_SPEAKERS)
		/* squirreldoc (const)
		*
		* Represents ignoring speaker arrangement.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_SPEAKERS
		*
		*/
		.Const("BASS_DEVICE_NOSPEAKER", BASS_DEVICE_NOSPEAKER)
		/* squirreldoc (const)
		*
		* Represents using ALSA "dmix" plugin.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_SPEAKERS
		*
		*/
		.Const("BASS_DEVICE_DMIX", BASS_DEVICE_DMIX)
		/* squirreldoc (const)
		*
		* Represents setting device's output rate to freq.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_FREQ
		*
		*/
		.Const("BASS_DEVICE_FREQ", BASS_DEVICE_FREQ)

		/* squirreldoc (const)
		*
		* Represents IDirectSound interface id.
		*
		* @category Object
		* @side		client
		* @name		BASS_OBJECT_DS
		*
		*/
		.Const("BASS_OBJECT_DS", BASS_OBJECT_DS)
		/* squirreldoc (const)
		*
		* Represents IDirectSound3DListener interface id.
		*
		* @category Object
		* @side		client
		* @name		BASS_OBJECT_DS3DL
		*
		*/
		.Const("BASS_OBJECT_DS3DL", BASS_OBJECT_DS3DL)

		/* squirreldoc (const)
		*
		* Represents that device is already enabled.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_ENABLED
		*
		*/
		.Const("BASS_DEVICE_ENABLED", BASS_DEVICE_ENABLED)
		/* squirreldoc (const)
		*
		* Represents using default system device.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_DEFAULT
		*
		*/
		.Const("BASS_DEVICE_DEFAULT", BASS_DEVICE_DEFAULT)
		/* squirreldoc (const)
		*
		* Represents that device is already initialized.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_INIT
		*
		*/
		.Const("BASS_DEVICE_INIT", BASS_DEVICE_INIT)

		.Const("BASS_DEVICE_TYPE_MASK", static_cast<SQInteger>(BASS_DEVICE_TYPE_MASK))
		.Const("BASS_DEVICE_TYPE_NETWORK", BASS_DEVICE_TYPE_NETWORK)
		.Const("BASS_DEVICE_TYPE_SPEAKERS", BASS_DEVICE_TYPE_SPEAKERS)
		.Const("BASS_DEVICE_TYPE_LINE", BASS_DEVICE_TYPE_LINE)
		.Const("BASS_DEVICE_TYPE_HEADPHONES", BASS_DEVICE_TYPE_HEADPHONES)
		.Const("BASS_DEVICE_TYPE_MICROPHONE", BASS_DEVICE_TYPE_MICROPHONE)
		.Const("BASS_DEVICE_TYPE_HEADSET", BASS_DEVICE_TYPE_HEADSET)
		.Const("BASS_DEVICE_TYPE_HANDSET", BASS_DEVICE_TYPE_HANDSET)
		.Const("BASS_DEVICE_TYPE_DIGITAL", BASS_DEVICE_TYPE_DIGITAL)
		.Const("BASS_DEVICE_TYPE_SPDIF", BASS_DEVICE_TYPE_SPDIF)
		.Const("BASS_DEVICE_TYPE_HDMI", BASS_DEVICE_TYPE_HDMI)
		.Const("BASS_DEVICE_TYPE_DISPLAYPORT", BASS_DEVICE_TYPE_DISPLAYPORT)

		.Const("DSCAPS_CONTINUOUSRATE", DSCAPS_CONTINUOUSRATE)
		.Const("DSCAPS_EMULDRIVER", DSCAPS_EMULDRIVER)
		.Const("DSCAPS_CERTIFIED", DSCAPS_CERTIFIED)
		.Const("DSCAPS_SECONDARYMONO", DSCAPS_SECONDARYMONO)
		.Const("DSCAPS_SECONDARYSTEREO", DSCAPS_SECONDARYSTEREO)
		.Const("DSCAPS_SECONDARY8BIT", DSCAPS_SECONDARY8BIT)
		.Const("DSCAPS_SECONDARY16BIT", DSCAPS_SECONDARY16BIT)

		.Const("BASS_SAMPLE_8BITS", BASS_SAMPLE_8BITS)
		.Const("BASS_SAMPLE_FLOAT", BASS_SAMPLE_FLOAT)
		.Const("BASS_SAMPLE_MONO", BASS_SAMPLE_MONO)
		.Const("BASS_SAMPLE_LOOP", BASS_SAMPLE_LOOP)
		.Const("BASS_SAMPLE_3D", BASS_SAMPLE_3D)
		.Const("BASS_SAMPLE_SOFTWARE", BASS_SAMPLE_SOFTWARE)
		.Const("BASS_SAMPLE_MUTEMAX", BASS_SAMPLE_MUTEMAX)
		.Const("BASS_SAMPLE_VAM", BASS_SAMPLE_VAM)
		.Const("BASS_SAMPLE_FX", BASS_SAMPLE_FX)
		.Const("BASS_SAMPLE_OVER_VOL", BASS_SAMPLE_OVER_VOL)
		.Const("BASS_SAMPLE_OVER_POS", BASS_SAMPLE_OVER_POS)
		.Const("BASS_SAMPLE_OVER_DIST", BASS_SAMPLE_OVER_DIST)

		.Const("BASS_STREAM_PRESCAN", BASS_STREAM_PRESCAN)
		.Const("BASS_MP3_SETPOS", BASS_MP3_SETPOS)
		.Const("BASS_STREAM_AUTOFREE", BASS_STREAM_AUTOFREE)
		.Const("BASS_STREAM_RESTRATE", BASS_STREAM_RESTRATE)
		.Const("BASS_STREAM_BLOCK", BASS_STREAM_BLOCK)
		.Const("BASS_STREAM_DECODE", BASS_STREAM_DECODE)
		.Const("BASS_STREAM_STATUS", BASS_STREAM_STATUS)

		.Const("BASS_MUSIC_FLOAT", BASS_MUSIC_FLOAT)
		.Const("BASS_MUSIC_MONO", BASS_MUSIC_MONO)
		.Const("BASS_MUSIC_LOOP", BASS_MUSIC_LOOP)
		.Const("BASS_MUSIC_3D", BASS_MUSIC_3D)
		.Const("BASS_MUSIC_FX", BASS_MUSIC_FX)
		.Const("BASS_MUSIC_AUTOFREE", BASS_MUSIC_AUTOFREE)
		.Const("BASS_MUSIC_DECODE", BASS_MUSIC_DECODE)
		.Const("BASS_MUSIC_PRESCAN", BASS_MUSIC_PRESCAN)
		.Const("BASS_MUSIC_CALCLEN", BASS_MUSIC_CALCLEN)
		.Const("BASS_MUSIC_RAMP", BASS_MUSIC_RAMP)
		.Const("BASS_MUSIC_RAMPS", BASS_MUSIC_RAMPS)
		.Const("BASS_MUSIC_SURROUND", BASS_MUSIC_SURROUND)
		.Const("BASS_MUSIC_SURROUND2", BASS_MUSIC_SURROUND2)
		.Const("BASS_MUSIC_FT2MOD", BASS_MUSIC_FT2MOD)
		.Const("BASS_MUSIC_PT1MOD", BASS_MUSIC_PT1MOD)
		.Const("BASS_MUSIC_NONINTER", BASS_MUSIC_NONINTER)
		.Const("BASS_MUSIC_SINCINTER", BASS_MUSIC_SINCINTER)
		.Const("BASS_MUSIC_POSRESET", BASS_MUSIC_POSRESET)
		.Const("BASS_MUSIC_POSRESETEX", BASS_MUSIC_POSRESETEX)
		.Const("BASS_MUSIC_STOPBACK", BASS_MUSIC_STOPBACK)
		.Const("BASS_MUSIC_NOSAMPLE", BASS_MUSIC_NOSAMPLE)

		.Const("BASS_SPEAKER_FRONT", BASS_SPEAKER_FRONT)
		.Const("BASS_SPEAKER_REAR", BASS_SPEAKER_REAR)
		.Const("BASS_SPEAKER_CENLFE", BASS_SPEAKER_CENLFE)
		.Const("BASS_SPEAKER_REAR2", BASS_SPEAKER_REAR2)
		.Const("BASS_SPEAKER_LEFT", BASS_SPEAKER_LEFT)
		.Const("BASS_SPEAKER_RIGHT", BASS_SPEAKER_RIGHT)
		.Const("BASS_SPEAKER_FRONTLEFT", BASS_SPEAKER_FRONTLEFT)
		.Const("BASS_SPEAKER_FRONTRIGHT", BASS_SPEAKER_FRONTRIGHT)
		.Const("BASS_SPEAKER_REARLEFT", BASS_SPEAKER_REARLEFT)
		.Const("BASS_SPEAKER_REARRIGHT", BASS_SPEAKER_REARRIGHT)
		.Const("BASS_SPEAKER_CENTER", BASS_SPEAKER_CENTER)
		.Const("BASS_SPEAKER_LFE", BASS_SPEAKER_LFE)
		.Const("BASS_SPEAKER_REAR2LEFT", BASS_SPEAKER_REAR2LEFT)
		.Const("BASS_SPEAKER_REAR2RIGHT", BASS_SPEAKER_REAR2RIGHT)

		.Const("BASS_ASYNCFILE", BASS_ASYNCFILE)
		.Const("BASS_UNICODE", static_cast<SQInteger>(BASS_UNICODE))

		.Const("BASS_VAM_HARDWARE", BASS_VAM_HARDWARE)
		.Const("BASS_VAM_SOFTWARE", BASS_VAM_SOFTWARE)
		.Const("BASS_VAM_TERM_TIME", BASS_VAM_TERM_TIME)
		.Const("BASS_VAM_TERM_DIST", BASS_VAM_TERM_DIST)
		.Const("BASS_VAM_TERM_PRIO", BASS_VAM_TERM_PRIO)

		.Const("BASS_CTYPE_SAMPLE", BASS_CTYPE_SAMPLE)
		.Const("BASS_CTYPE_STREAM", BASS_CTYPE_STREAM)
		.Const("BASS_CTYPE_STREAM_OGG", BASS_CTYPE_STREAM_OGG)
		.Const("BASS_CTYPE_STREAM_MP1", BASS_CTYPE_STREAM_MP1)
		.Const("BASS_CTYPE_STREAM_MP2", BASS_CTYPE_STREAM_MP2)
		.Const("BASS_CTYPE_STREAM_MP3", BASS_CTYPE_STREAM_MP3)
		.Const("BASS_CTYPE_STREAM_AIFF", BASS_CTYPE_STREAM_AIFF)
		.Const("BASS_CTYPE_STREAM_CA", BASS_CTYPE_STREAM_CA)
		.Const("BASS_CTYPE_STREAM_MF", BASS_CTYPE_STREAM_MF)
		.Const("BASS_CTYPE_STREAM_WAV", BASS_CTYPE_STREAM_WAV)
		.Const("BASS_CTYPE_STREAM_WAV_PCM", BASS_CTYPE_STREAM_WAV_PCM)
		.Const("BASS_CTYPE_STREAM_WAV_FLOAT", BASS_CTYPE_STREAM_WAV_FLOAT)
		.Const("BASS_CTYPE_MUSIC_MOD", BASS_CTYPE_MUSIC_MOD)
		.Const("BASS_CTYPE_MUSIC_MTM", BASS_CTYPE_MUSIC_MTM)
		.Const("BASS_CTYPE_MUSIC_S3M", BASS_CTYPE_MUSIC_S3M)
		.Const("BASS_CTYPE_MUSIC_XM", BASS_CTYPE_MUSIC_XM)
		.Const("BASS_CTYPE_MUSIC_IT", BASS_CTYPE_MUSIC_IT)
		.Const("BASS_CTYPE_MUSIC_MO3", BASS_CTYPE_MUSIC_MO3)

		.Const("BASS_3DMODE_NORMAL", BASS_3DMODE_NORMAL)
		.Const("BASS_3DMODE_RELATIVE", BASS_3DMODE_RELATIVE)
		.Const("BASS_3DMODE_OFF", BASS_3DMODE_OFF)

		.Const("BASS_3DALG_DEFAULT", BASS_3DALG_DEFAULT)
		.Const("BASS_3DALG_OFF", BASS_3DALG_OFF)
		.Const("BASS_3DALG_FULL", BASS_3DALG_FULL)
		.Const("BASS_3DALG_LIGHT", BASS_3DALG_LIGHT)

		/* squirreldoc (const)
		*
		* Represents generic enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_GENERIC
		*
		*/
		.Const("EAX_ENVIRONMENT_GENERIC", EAX_ENVIRONMENT_GENERIC)
		/* squirreldoc (const)
		*
		* Represents padded cell enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_PADDEDCELL
		*
		*/
		.Const("EAX_ENVIRONMENT_PADDEDCELL", EAX_ENVIRONMENT_PADDEDCELL)
		/* squirreldoc (const)
		*
		* Represents room enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_ROOM
		*
		*/
		.Const("EAX_ENVIRONMENT_ROOM", EAX_ENVIRONMENT_ROOM)
		/* squirreldoc (const)
		*
		* Represents bathroom enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_BATHROOM
		*
		*/
		.Const("EAX_ENVIRONMENT_BATHROOM", EAX_ENVIRONMENT_BATHROOM)
		/* squirreldoc (const)
		*
		* Represents living room enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_LIVINGROOM
		*
		*/
		.Const("EAX_ENVIRONMENT_LIVINGROOM", EAX_ENVIRONMENT_LIVINGROOM)
		/* squirreldoc (const)
		*
		* Represents stone room enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_STONEROOM
		*
		*/
		.Const("EAX_ENVIRONMENT_STONEROOM", EAX_ENVIRONMENT_STONEROOM)
		/* squirreldoc (const)
		*
		* Represents auditorium enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_AUDITORIUM
		*
		*/
		.Const("EAX_ENVIRONMENT_AUDITORIUM", EAX_ENVIRONMENT_AUDITORIUM)
		/* squirreldoc (const)
		*
		* Represents concert hall enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_CONCERTHALL
		*
		*/
		.Const("EAX_ENVIRONMENT_CONCERTHALL", EAX_ENVIRONMENT_CONCERTHALL)
		/* squirreldoc (const)
		*
		* Represents cave enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_CAVE
		*
		*/
		.Const("EAX_ENVIRONMENT_CAVE", EAX_ENVIRONMENT_CAVE)
		/* squirreldoc (const)
		*
		* Represents arena enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_ARENA
		*
		*/
		.Const("EAX_ENVIRONMENT_ARENA", EAX_ENVIRONMENT_ARENA)
		/* squirreldoc (const)
		*
		* Represents hangar enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_HANGAR
		*
		*/
		.Const("EAX_ENVIRONMENT_HANGAR", EAX_ENVIRONMENT_HANGAR)
		/* squirreldoc (const)
		*
		* Represents carpeted hallway enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_CARPETEDHALLWAY
		*
		*/
		.Const("EAX_ENVIRONMENT_CARPETEDHALLWAY", EAX_ENVIRONMENT_CARPETEDHALLWAY)
		/* squirreldoc (const)
		*
		* Represents hallway enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_HALLWAY
		*
		*/
		.Const("EAX_ENVIRONMENT_HALLWAY", EAX_ENVIRONMENT_HALLWAY)
		/* squirreldoc (const)
		*
		* Represents stone corridor enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_STONECORRIDOR
		*
		*/
		.Const("EAX_ENVIRONMENT_STONECORRIDOR", EAX_ENVIRONMENT_STONECORRIDOR)
		/* squirreldoc (const)
		*
		* Represents alley enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_ALLEY
		*
		*/
		.Const("EAX_ENVIRONMENT_ALLEY", EAX_ENVIRONMENT_ALLEY)
		/* squirreldoc (const)
		*
		* Represents forest enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_FOREST
		*
		*/
		.Const("EAX_ENVIRONMENT_FOREST", EAX_ENVIRONMENT_FOREST)
		/* squirreldoc (const)
		*
		* Represents city enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_CITY
		*
		*/
		.Const("EAX_ENVIRONMENT_CITY", EAX_ENVIRONMENT_CITY)
		/* squirreldoc (const)
		*
		* Represents mountains enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_MOUNTAINS
		*
		*/
		.Const("EAX_ENVIRONMENT_MOUNTAINS", EAX_ENVIRONMENT_MOUNTAINS)
		/* squirreldoc (const)
		*
		* Represents quarry enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_QUARRY
		*
		*/
		.Const("EAX_ENVIRONMENT_QUARRY", EAX_ENVIRONMENT_QUARRY)
		/* squirreldoc (const)
		*
		* Represents plain enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_PLAIN
		*
		*/
		.Const("EAX_ENVIRONMENT_PLAIN", EAX_ENVIRONMENT_PLAIN)
		/* squirreldoc (const)
		*
		* Represents parking lot enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_PARKINGLOT
		*
		*/
		.Const("EAX_ENVIRONMENT_PARKINGLOT", EAX_ENVIRONMENT_PARKINGLOT)
		/* squirreldoc (const)
		*
		* Represents sewer pipe enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_SEWERPIPE
		*
		*/
		.Const("EAX_ENVIRONMENT_SEWERPIPE", EAX_ENVIRONMENT_SEWERPIPE)
		/* squirreldoc (const)
		*
		* Represents underwater enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_UNDERWATER
		*
		*/
		.Const("EAX_ENVIRONMENT_UNDERWATER", EAX_ENVIRONMENT_UNDERWATER)
		/* squirreldoc (const)
		*
		* Represents drugged enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_DRUGGED
		*
		*/
		.Const("EAX_ENVIRONMENT_DRUGGED", EAX_ENVIRONMENT_DRUGGED)
		/* squirreldoc (const)
		*
		* Represents dizzy enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_DIZZY
		*
		*/
		.Const("EAX_ENVIRONMENT_DIZZY", EAX_ENVIRONMENT_DIZZY)
		/* squirreldoc (const)
		*
		* Represents psychotic enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_PSYCHOTIC
		*
		*/
		.Const("EAX_ENVIRONMENT_PSYCHOTIC", EAX_ENVIRONMENT_PSYCHOTIC)

		.Const("STREAMFILE_NOBUFFER", STREAMFILE_NOBUFFER)
		.Const("STREAMFILE_BUFFER", STREAMFILE_BUFFER)
		.Const("STREAMFILE_BUFFERPUSH", STREAMFILE_BUFFERPUSH)

		.Const("BASS_FILEPOS_CURRENT", BASS_FILEPOS_CURRENT)
		.Const("BASS_FILEPOS_DECODE", BASS_FILEPOS_DECODE)
		.Const("BASS_FILEPOS_DOWNLOAD", BASS_FILEPOS_DOWNLOAD)
		.Const("BASS_FILEPOS_END", BASS_FILEPOS_END)
		.Const("BASS_FILEPOS_START", BASS_FILEPOS_START)
		.Const("BASS_FILEPOS_CONNECTED", BASS_FILEPOS_CONNECTED)
		.Const("BASS_FILEPOS_BUFFER", BASS_FILEPOS_BUFFER)
		.Const("BASS_FILEPOS_SOCKET", BASS_FILEPOS_SOCKET)
		.Const("BASS_FILEPOS_ASYNCBUF", BASS_FILEPOS_ASYNCBUF)
		.Const("BASS_FILEPOS_SIZE", BASS_FILEPOS_SIZE)

		.Const("BASS_ACTIVE_STOPPED", BASS_ACTIVE_STOPPED)
		.Const("BASS_ACTIVE_PLAYING", BASS_ACTIVE_PLAYING)
		.Const("BASS_ACTIVE_STALLED", BASS_ACTIVE_STALLED)
		.Const("BASS_ACTIVE_PAUSED", BASS_ACTIVE_PAUSED)

		.Const("BASS_ATTRIB_FREQ", BASS_ATTRIB_FREQ)
		.Const("BASS_ATTRIB_VOL", BASS_ATTRIB_VOL)
		.Const("BASS_ATTRIB_PAN", BASS_ATTRIB_PAN)
		.Const("BASS_ATTRIB_EAXMIX", BASS_ATTRIB_EAXMIX)
		.Const("BASS_ATTRIB_NOBUFFER", BASS_ATTRIB_NOBUFFER)
		.Const("BASS_ATTRIB_VBR", BASS_ATTRIB_VBR)
		.Const("BASS_ATTRIB_CPU", BASS_ATTRIB_CPU)
		.Const("BASS_ATTRIB_SRC", BASS_ATTRIB_SRC)
		.Const("BASS_ATTRIB_NET_RESUME", BASS_ATTRIB_NET_RESUME)
		.Const("BASS_ATTRIB_SCANINFO", BASS_ATTRIB_SCANINFO)
		.Const("BASS_ATTRIB_MUSIC_AMPLIFY", BASS_ATTRIB_MUSIC_AMPLIFY)
		.Const("BASS_ATTRIB_MUSIC_PANSEP", BASS_ATTRIB_MUSIC_PANSEP)
		.Const("BASS_ATTRIB_MUSIC_PSCALER", BASS_ATTRIB_MUSIC_PSCALER)
		.Const("BASS_ATTRIB_MUSIC_BPM", BASS_ATTRIB_MUSIC_BPM)
		.Const("BASS_ATTRIB_MUSIC_SPEED", BASS_ATTRIB_MUSIC_SPEED)
		.Const("BASS_ATTRIB_MUSIC_VOL_GLOBAL", BASS_ATTRIB_MUSIC_VOL_GLOBAL)
		.Const("BASS_ATTRIB_MUSIC_ACTIVE", BASS_ATTRIB_MUSIC_ACTIVE)
		.Const("BASS_ATTRIB_MUSIC_VOL_CHAN", BASS_ATTRIB_MUSIC_VOL_CHAN)
		.Const("BASS_ATTRIB_MUSIC_VOL_INST", BASS_ATTRIB_MUSIC_VOL_INST)

		.Const("BASS_LEVEL_MONO", BASS_LEVEL_MONO)
		.Const("BASS_LEVEL_STEREO", BASS_LEVEL_STEREO)
		.Const("BASS_LEVEL_RMS", BASS_LEVEL_RMS)

		.Const("BASS_TAG_ID3", BASS_TAG_ID3)
		.Const("BASS_TAG_ID3V2", BASS_TAG_ID3V2)
		.Const("BASS_TAG_OGG", BASS_TAG_OGG)
		.Const("BASS_TAG_HTTP", BASS_TAG_HTTP)
		.Const("BASS_TAG_ICY", BASS_TAG_ICY)
		.Const("BASS_TAG_META", BASS_TAG_META)
		.Const("BASS_TAG_APE", BASS_TAG_APE)
		.Const("BASS_TAG_MP4", BASS_TAG_MP4)
		.Const("BASS_TAG_VENDOR", BASS_TAG_VENDOR)
		.Const("BASS_TAG_LYRICS3", BASS_TAG_LYRICS3)
		.Const("BASS_TAG_CA_CODEC", BASS_TAG_CA_CODEC)
		.Const("BASS_TAG_MF", BASS_TAG_MF)
		.Const("BASS_TAG_WAVEFORMAT", BASS_TAG_WAVEFORMAT)
		.Const("BASS_TAG_RIFF_INFO", BASS_TAG_RIFF_INFO)
		.Const("BASS_TAG_RIFF_BEXT", BASS_TAG_RIFF_BEXT)
		.Const("BASS_TAG_RIFF_CART", BASS_TAG_RIFF_CART)
		.Const("BASS_TAG_RIFF_DISP", BASS_TAG_RIFF_DISP)
		.Const("BASS_TAG_APE_BINARY", BASS_TAG_APE_BINARY)
		.Const("BASS_TAG_MUSIC_NAME", BASS_TAG_MUSIC_NAME)
		.Const("BASS_TAG_MUSIC_MESSAGE", BASS_TAG_MUSIC_MESSAGE)
		.Const("BASS_TAG_MUSIC_ORDERS", BASS_TAG_MUSIC_ORDERS)
		.Const("BASS_TAG_MUSIC_INST", BASS_TAG_MUSIC_INST)
		.Const("BASS_TAG_MUSIC_SAMPLE", BASS_TAG_MUSIC_SAMPLE)

		.Const("BASS_POS_BYTE", BASS_POS_BYTE)
		.Const("BASS_POS_MUSIC_ORDER", BASS_POS_MUSIC_ORDER)
		.Const("BASS_POS_OGG", BASS_POS_OGG)
		.Const("BASS_POS_INEXACT", BASS_POS_INEXACT)
		.Const("BASS_POS_DECODE", BASS_POS_DECODE)
		.Const("BASS_POS_DECODETO", BASS_POS_DECODETO)
		.Const("BASS_POS_SCAN", BASS_POS_SCAN)

		.Const("BASS_INPUT_OFF", BASS_INPUT_OFF)
		.Const("BASS_INPUT_ON", BASS_INPUT_ON)

		.Const("BASS_INPUT_TYPE_MASK", static_cast<SQInteger>(BASS_INPUT_TYPE_MASK))
		.Const("BASS_INPUT_TYPE_UNDEF", BASS_INPUT_TYPE_UNDEF)
		.Const("BASS_INPUT_TYPE_DIGITAL", BASS_INPUT_TYPE_DIGITAL)
		.Const("BASS_INPUT_TYPE_LINE", BASS_INPUT_TYPE_LINE)
		.Const("BASS_INPUT_TYPE_MIC", BASS_INPUT_TYPE_MIC)
		.Const("BASS_INPUT_TYPE_SYNTH", BASS_INPUT_TYPE_SYNTH)
		.Const("BASS_INPUT_TYPE_SYNTH", BASS_INPUT_TYPE_SYNTH)
		.Const("BASS_INPUT_TYPE_CD", BASS_INPUT_TYPE_CD)
		.Const("BASS_INPUT_TYPE_PHONE", BASS_INPUT_TYPE_PHONE)
		.Const("BASS_INPUT_TYPE_SPEAKER", BASS_INPUT_TYPE_SPEAKER)
		.Const("BASS_INPUT_TYPE_WAVE", BASS_INPUT_TYPE_WAVE)
		.Const("BASS_INPUT_TYPE_AUX", BASS_INPUT_TYPE_AUX)
		.Const("BASS_INPUT_TYPE_ANALOG", BASS_INPUT_TYPE_ANALOG)

		.Const("BASS_FX_DX8_CHORUS", BASS_FX_DX8_CHORUS)
		.Const("BASS_FX_DX8_COMPRESSOR", BASS_FX_DX8_COMPRESSOR)
		.Const("BASS_FX_DX8_DISTORTION", BASS_FX_DX8_DISTORTION)
		.Const("BASS_FX_DX8_ECHO", BASS_FX_DX8_ECHO)
		.Const("BASS_FX_DX8_FLANGER", BASS_FX_DX8_FLANGER)
		.Const("BASS_FX_DX8_GARGLE", BASS_FX_DX8_GARGLE)
		.Const("BASS_FX_DX8_I3DL2REVERB", BASS_FX_DX8_I3DL2REVERB)
		.Const("BASS_FX_DX8_PARAMEQ", BASS_FX_DX8_PARAMEQ)
		.Const("BASS_FX_DX8_REVERB", BASS_FX_DX8_REVERB)
	;

	return SQ_OK;
}
