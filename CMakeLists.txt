cmake_minimum_required(VERSION 3.17)

project(sq_bass)

option(INSTALL_AFTER_BUILD "Run cmake --install separately for every target after each build. By default this option is set to OFF" OFF)

set(GAME_PATH "" CACHE PATH "This option specifies the game location. It's only used for the installation step.")
set(SERVER_PATH "" CACHE PATH "This option specifies the server location. It's only used for the installation step.")

file(GLOB SRC
	"src/api/squirrel_api.h"
	"src/api/module_api.h"
	"src/api/module_api.cpp"

	"src/bass_error_message.h"
	"src/bass_error_message.cpp"
	"src/bass_vdfs.h"
	"src/bass_vdfs.cpp"
	"src/sqFunction.h"
	"src/sqFunction.cpp"
	"src/sqmain.cpp"
	"src/sqratGlobalFuncStdcall.h"
	"src/sqUtils.h"
	"src/sqUtils.cpp"
)

add_library(sq_bass SHARED ${SRC})

target_compile_definitions(sq_bass
	PRIVATE
		SCRAT_EXPORT
)

target_precompile_headers(sq_bass
	PRIVATE
		"src/api/module_api.h"
		"src/api/squirrel_api.h"
)

target_include_directories(sq_bass
	INTERFACE
		"include/"
	PRIVATE
		"src/"
)

add_subdirectory(dependencies)

if(DEFINED OUT_FILE_SUFFIX)
	set_target_properties(sq_bass
			PROPERTIES 
				PREFIX ""
				SUFFIX ".${OUT_FILE_SUFFIX}${CMAKE_SHARED_LIBRARY_SUFFIX}"
	)
endif()

if (NOT ${GAME_PATH} STREQUAL "")
	install(TARGETS sq_bass
			RUNTIME
			  DESTINATION ${GAME_PATH}
			  COMPONENT "clientModule"
	)

	if(INSTALL_AFTER_BUILD)
		add_custom_command(TARGET sq_bass 
			POST_BUILD
				COMMAND ${CMAKE_COMMAND} --install ${CMAKE_CURRENT_BINARY_DIR} --component "clientModule"
		)
	endif()
endif()

if (NOT ${SERVER_PATH} STREQUAL "")
	install(TARGETS sq_bass
			RUNTIME
			  DESTINATION ${SERVER_PATH}
			  COMPONENT "serverModule"
	)

	if(INSTALL_AFTER_BUILD)
		add_custom_command(TARGET sq_bass 
			POST_BUILD
				COMMAND ${CMAKE_COMMAND} --install ${CMAKE_CURRENT_BINARY_DIR} --component "serverModule"
		)
	endif()
endif()
